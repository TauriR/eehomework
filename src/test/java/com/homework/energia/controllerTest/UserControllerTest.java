package com.homework.energia.controllerTest;

import com.homework.energia.controller.UserController;
import com.homework.energia.repository.UserRepository;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.ui.Model;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;



public class UserControllerTest {
    private static UserController userController;
    private static Model mockedModel;
    private static UserRepository mockedUserRepository;


    @BeforeClass
    public static void UserControllerInstance(){
        mockedModel = mock(Model.class);
        mockedUserRepository = mock(UserRepository.class);
        userController = new UserController(mockedUserRepository);
}

    @Test(expected = IllegalArgumentException.class)
    public void testDeleteUser(){
    assertThat(userController.deleteUser(1l,mockedModel)).isEqualTo("index");
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestUpdateForm() {
        assertThat(userController.showUpdateForm(0, mockedModel)).isEqualTo("update-user");
    }

}
