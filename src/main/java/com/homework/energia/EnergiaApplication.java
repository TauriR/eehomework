package com.homework.energia;

import com.homework.energia.model.User;
import com.homework.energia.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
public class EnergiaApplication implements CommandLineRunner {
private final UserRepository userRepository;
@Autowired
    public EnergiaApplication(UserRepository userService, UserRepository userRepository) {
    this.userRepository = userRepository;

    }

    public static void main(String[] args) {
        SpringApplication.run(EnergiaApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        User usr = new User();
        usr.setFirstName("Ain");
        usr.setLastName("Kala");
        usr.setBirthDate("11.01.1900");
        usr.setEmail("ain.kala@gmail.com");
        usr.setAddress("viiralti 13 Tallinn");
        System.out.println(userRepository.save(usr));

        User secUser = new User();
        secUser.setFirstName("Siiru");
        secUser.setLastName("Viiru");
        secUser.setBirthDate("11.01.1980");
        secUser.setEmail("siiru.viiru@gmail.com");
        secUser.setAddress("Soo 10 Tallinn");
        System.out.println(userRepository.save(secUser));

        User thirdUser = new User();
        thirdUser.setFirstName("Heli");
        thirdUser.setLastName("Kopter");
        thirdUser.setBirthDate("14.08.1907");
        thirdUser.setEmail("heli.kopter@gmail.com");
        thirdUser.setAddress("Prantsusmaa");
        System.out.println(userRepository.save(thirdUser));
    }



}
